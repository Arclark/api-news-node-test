# News Nest Demo API

# Requerimientos

- [Node.js](https://nodejs.org/es/download/) (probado en última LTS: `14.17.5`)
- [Docker](https://docs.docker.com/get-docker/) (Esto es por si quieres agilizar el proceso de despliegue)
- [Docker Compose](https://docs.docker.com/compose/install/) (Permite ejecutar facilmente una orquestación de contenedores)

- [PostgreSQL](https://www.prisma.io/docs/) (Gestor administrado por TypeOrM)

# Clonar repositorio

- Clonar el repositorio utilizando `git clone git@gitlab.com:Arclark/api-news-node-test.git`
- Crea el archivo `.env`, tomando como ejemplo el archivo `.env-example`

# Ejecutando con Docker 🐳

- Ejecuta el comando `docker-compose up` para correr los contenedores y listo :)

# Ejecutando en local 💻

- Ejecuta `cd app/` para ingresar a la carpeta del proyecto.
- Ejecuta `npm install` para instalar las dependencias.
- Ejecuta `npm run migrations:run` para crear las tablas de la Base de Datos.
- Ejecuta `npm run start:dev` para correr localmente tu proyecto en el puerto 3000.

# Tareas

- [x] Configuración inicial del proyecto.
- [x] Implementación de base de datos.
- [x] Sistema de migraciones de base de datos.
- [x] Configuración de contenedores Docker para desarrollo.
- [x] Estrategia de despliegue a development y production.
- [x] Autenticación por JWT.
- [x] CRUD de usuarios.
- [x] Documentación con Swagger.
- [ ] Mocks en tests.
- [ ] Test unitarios.
