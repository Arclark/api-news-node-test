import { Injectable, Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config'; // 👈 Import ConfigType
import config from './config'; // 👈 config file
@Injectable()
export class AppService {
  constructor(
    @Inject(config.KEY) private configService: ConfigType<typeof config>, // 👈
  ) {}
  getHello(): string {
    return `Hello World! ${this.configService.database.db_name}`;
  }
}
