import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    database: {
      db_name: process.env.DB_NAME,
      db_port: process.env.DB_PORT,
      db_host: process.env.DB_HOST,
      db_password: process.env.DB_PASSWORD,
      db_user: process.env.DB_USER,
    },
    jwt: {
      secret: process.env.JWT_SECRET,
    },
    api: {
      path: process.env.API_PATH,
    },
  };
});
