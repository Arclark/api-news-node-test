import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionOptions } from 'typeorm';
import config from '../config';
@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      // 👈 use TypeOrmModule
      inject: [config.KEY],
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { db_user, db_host, db_name, db_password, db_port } =
          configService.database;

        return {
          type: 'postgres',
          host: db_host,
          port: +db_port,
          username: db_user,
          password: db_password,
          database: db_name,
          synchronize: false, // 👈 new attr
          autoLoadEntities: true,
        } as ConnectionOptions;
      },
    }),
  ],
  exports: [TypeOrmModule], // 👈 add in exports
})
export class DatabaseModule {}
