import {MigrationInterface, QueryRunner} from "typeorm";

export class init1639721817258 implements MigrationInterface {
    name = 'init1639721817258'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "name" character varying(25) NOT NULL, "lastname" character varying(25) NOT NULL, "email" character varying(50) NOT NULL, "username" character varying(15) NOT NULL, "password" character varying(64) NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
