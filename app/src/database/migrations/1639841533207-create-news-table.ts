import {MigrationInterface, QueryRunner} from "typeorm";

export class createNewsTable1639841533207 implements MigrationInterface {
    name = 'createNewsTable1639841533207'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "news" ("id" SERIAL NOT NULL, "objectID" integer NOT NULL, "title" character varying(255) NOT NULL, "url" text NOT NULL, "author" character varying(255) NOT NULL, "points" integer NOT NULL DEFAULT '0', "story_text" text NOT NULL, "comment_text" text NOT NULL, "num_comments" integer NOT NULL DEFAULT '0', "story_id" integer NOT NULL, "story_title" character varying(255) NOT NULL, "story_url" text NOT NULL, "parent_id" integer NOT NULL, "_tags" json NOT NULL, "created_at" TIMESTAMP NOT NULL, CONSTRAINT "PK_39a43dfcb6007180f04aff2357e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "news"`);
    }

}
