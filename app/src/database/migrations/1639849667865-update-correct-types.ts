import {MigrationInterface, QueryRunner} from "typeorm";

export class updateCorrectTypes1639849667865 implements MigrationInterface {
    name = 'updateCorrectTypes1639849667865'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "title" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "url" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_text" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_url" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "parent_id" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "parent_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_url" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_text" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "url" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "title" SET NOT NULL`);
    }

}
