import {MigrationInterface, QueryRunner} from "typeorm";

export class nullableCorrection1639850460312 implements MigrationInterface {
    name = 'nullableCorrection1639850460312'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "points" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "num_comments" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "num_comments" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "points" SET NOT NULL`);
    }

}
