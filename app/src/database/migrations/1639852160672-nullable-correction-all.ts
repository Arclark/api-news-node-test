import {MigrationInterface, QueryRunner} from "typeorm";

export class nullableCorrectionAll1639852160672 implements MigrationInterface {
    name = 'nullableCorrectionAll1639852160672'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "author" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "comment_text" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_title" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "_tags" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "created_at" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "created_at" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "_tags" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_title" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "story_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "comment_text" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "author" SET NOT NULL`);
    }

}
