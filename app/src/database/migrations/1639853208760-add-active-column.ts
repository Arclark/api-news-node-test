import {MigrationInterface, QueryRunner} from "typeorm";

export class addActiveColumn1639853208760 implements MigrationInterface {
    name = 'addActiveColumn1639853208760'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ADD "active" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "active"`);
    }

}
