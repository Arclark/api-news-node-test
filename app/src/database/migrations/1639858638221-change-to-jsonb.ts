import {MigrationInterface, QueryRunner} from "typeorm";

export class changeToJsonb1639858638221 implements MigrationInterface {
    name = 'changeToJsonb1639858638221'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "_tags"`);
        await queryRunner.query(`ALTER TABLE "news" ADD "_tags" jsonb`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "_tags"`);
        await queryRunner.query(`ALTER TABLE "news" ADD "_tags" json`);
    }

}
