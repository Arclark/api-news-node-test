import {MigrationInterface, QueryRunner} from "typeorm";

export class changeTypeString1639858693565 implements MigrationInterface {
    name = 'changeTypeString1639858693565'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "_tags"`);
        await queryRunner.query(`ALTER TABLE "news" ADD "_tags" jsonb`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "_tags"`);
        await queryRunner.query(`ALTER TABLE "news" ADD "_tags" json`);
    }

}
