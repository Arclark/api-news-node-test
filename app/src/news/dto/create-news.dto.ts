import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsJSON,
  IsDate,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateNewsDto {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: `objectID` })
  readonly objectID: number;

  @IsString()
  @ApiProperty({ description: `title` })
  readonly title: string;

  @IsString()
  @ApiProperty({ description: `url` })
  readonly url: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `author` })
  readonly author: string;

  @IsNumber()
  @ApiProperty({ description: `points` })
  readonly points: number;

  @IsString()
  @ApiProperty({ description: `story_text` })
  readonly story_text: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `comment_text` })
  readonly comment_text: string;

  @IsNumber()
  @ApiProperty({ description: `num_comments` })
  readonly num_comments: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: `story_id` })
  readonly story_id: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `story_title` })
  readonly story_title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `story_url` })
  readonly story_url: string;

  @IsNumber()
  @ApiProperty({ description: `parent_id` })
  readonly parent_id: number;

  @IsJSON()
  @IsNotEmpty()
  @ApiProperty({ description: `_tags` })
  readonly _tags: string;

  @IsDate()
  @IsNotEmpty()
  @ApiProperty({ description: `created_at` })
  readonly created_at: string;
}
