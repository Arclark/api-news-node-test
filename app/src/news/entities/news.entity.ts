import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';

@Entity()
export class News {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  objectID: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  title: string;

  @Column({ type: 'text', nullable: true })
  url: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  author: string;

  @Column({ type: 'int', default: 0, nullable: true })
  points: number;

  @Column({ type: 'text', nullable: true })
  story_text: string;

  @Column({ type: 'text', nullable: true })
  comment_text: string;

  @Column({ type: 'int', default: 0, nullable: true })
  num_comments: number;

  @Column({ type: 'int', nullable: true })
  story_id: number;

  @Column({ type: 'varchar', length: '255', nullable: true })
  story_title: string;

  @Column({ type: 'text', nullable: true })
  story_url: string;

  @Column({ type: 'int', nullable: true })
  parent_id: number;
  @Column({ type: 'boolean', default: true })
  active: boolean;
  @Column({ type: 'jsonb', nullable: true })
  _tags: string;

  @Column({ type: 'timestamp', nullable: true })
  created_at: string;
}
