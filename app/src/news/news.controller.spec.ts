import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;
  let service: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    }).compile();

    controller = module.get<NewsController>(NewsController);
  });

  describe('findAll', () => {
    it('should return an array of news paginates', async () => {
      const result = ['test'];
      jest
        .spyOn(service, 'findAll')
        .mockImplementation(async () => await result);

      expect(await controller.findAll()).toBe(result);
    });
  });
});
