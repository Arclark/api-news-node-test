import {
  Controller,
  Get,
  Param,
  Delete,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  UseGuards,
  Inject,
} from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { ApiBearerAuth, ApiTags, ApiQuery } from '@nestjs/swagger';
import { ConfigType } from '@nestjs/config';

import { NewsService } from './news.service';
import { News } from './entities/news.entity';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import config from '../config';
@UseGuards(JwtAuthGuard)
@ApiBearerAuth('JWT-auth')
@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
  ) {}
  @ApiQuery({
    name: 'page',
    description: 'page to navigate',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'limit',
    description: 'The maximum number of transactions to return',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'search ',
    description: 'filter to finding',
    required: false,
    type: String,
  })
  @Get()
  findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
    @Query('limit', new DefaultValuePipe(5), ParseIntPipe) limit = 5,
    @Query('search') search = '',
  ): Promise<Pagination<News>> {
    limit = limit > 100 ? 100 : limit;
    return this.newsService.findAll(
      {
        page,
        limit,
        route: `${this.configService.api.path}/news`,
      },
      search,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.newsService.disable(+id);
  }
}
