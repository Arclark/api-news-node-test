import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { News } from './entities/news.entity';
@Module({
  controllers: [NewsController],
  imports: [HttpModule, TypeOrmModule.forFeature([News])],
  providers: [NewsService],
})
export class NewsModule {}
