import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';

import { Cron } from '@nestjs/schedule';
import { News } from './entities/news.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { AxiosResponse } from 'axios';
import { map } from 'rxjs/operators';
import { Logger } from '@nestjs/common';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';
@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News) private newsRepo: Repository<News>, // 👈 Inject
    private httpService: HttpService,
  ) {}
  private createNewsDto: CreateNewsDto;
  async getDataApi() {
    const obj = await this.getEndpoint(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );

    return obj;
  }

  findAll(
    options: IPaginationOptions,
    search?: string,
  ): Promise<Pagination<News>> {
    const queryBuilder = this.newsRepo.createQueryBuilder('news');

    if (search) {
      queryBuilder
        .where('news.active= :active', { active: true })
        .andWhere('news.author like :author', { author: `%${search}%` })
        .orWhere('news.title like :title', { title: `%${search}%` })
        .orWhere(`news._tags ::jsonb @> \'"${search}"\'`)
        .orWhere("to_char(news.created_at,'MM') = :month ", {
          month: this.getNumbeMonth(search?.toLowerCase()),
        })
        .orderBy('news.created_at', 'DESC');
    } else {
      queryBuilder
        .where('news.active= :active', { active: true })
        .orderBy('news.created_at', 'DESC');
    }
    return paginate<News>(queryBuilder, options);
  }

  async disable(id: number) {
    const news = await this.newsRepo.findOne(id);
    if (news) {
      this.newsRepo.merge(news, { active: false });
      return this.newsRepo.save(news);
    } else {
      throw new NotFoundException('User not found');
    }
  }

  async create() {
    this.getDataApi().then(async (result) => {
      let newdata = result.hits;
      const objectsIDs = result.hits.map((element) => {
        return parseInt(element.objectID);
      });
      const newsSaves = await this.newsRepo.find({
        where: { objectID: In(objectsIDs) },
      });

      if (newsSaves.length > 0) {
        newsSaves.map((element) => {
          newdata = newdata.filter(
            (x) => parseInt(x.objectID) !== element.objectID,
          );
        });
      }

      if (newdata.length > 0) {
        const dataNewsSave = newdata.map((element) => {
          return {
            objectID: element.objectID,
            title: element.title,
            url: element.url,
            author: element.author,
            points: element.points,
            story_text: element.story_text,
            comment_text: element.comment_text,
            num_comments: element.num_comments,
            story_id: element.story_id,
            story_title: element.story_title,
            story_url: element.story_url,
            parent_id: element.parent_id,
            _tags: element._tags,
            created_at: element.created_at,
          };
        });

        let insertNews = this.newsRepo.create(this.createNewsDto);
        insertNews = dataNewsSave;
        this.newsRepo.insert(insertNews).then(() => {
          Logger.log('synchronised successfull');
        });
      }
    });
  }

  getNumbeMonth(name: string) {
    name.toLowerCase();
    switch (name) {
      case 'january':
        return '01';

      case 'february':
        return '02';

      case 'march':
        return '03';

      case 'april':
        return '04';

      case 'may':
        return '05';

      case 'june':
        return '06';

      case 'july':
        return '07';

      case 'august':
        return '08';

      case 'september':
        return '09';

      case 'october':
        return '10';

      case 'november':
        return '11';
      case 'december':
        return '12';
    }
  }
  private getEndpoint(url): Promise<any> {
    return lastValueFrom(
      this.httpService.get<AxiosResponse>(url).pipe(
        map((res) => {
          return res.data;
        }),
      ),
    );
  }

  @Cron('* * */1 * * *')
  handleCronJob() {
    this.create();
  }
}
