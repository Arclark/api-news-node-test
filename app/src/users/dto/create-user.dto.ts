import { IsString, IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `full name` })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `lastname` })
  readonly lastname: string;
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ description: `email` })
  readonly email: string;
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `username` })
  readonly username: string;
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: `password` })
  readonly password: string;
}
