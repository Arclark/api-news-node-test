import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';
import { Exclude } from 'class-transformer';
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'varchar', length: 25 })
  name: string;
  @Column({ type: 'varchar', length: 25 })
  lastname: string;
  @Column({
    type: 'varchar',
    length: 50,
    unique: true,
  })
  email: string;
  @Column({
    type: 'varchar',
    length: 15,
    unique: true,
  })
  username: string;

  @Column({ type: 'varchar', length: 64 })
  @Exclude({ toPlainOnly: false })
  password: string;
}
