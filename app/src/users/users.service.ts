import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as bcrypt from 'bcrypt';
import { User } from './entities/user.entity';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>, // 👈 Inject
  ) {}
  async create(createUserDto: CreateUserDto) {
    const newUser = this.userRepo.create(createUserDto);
    const hashPassword = await bcrypt.hash(newUser.password, 10);
    newUser.password = hashPassword;
    return this.userRepo.save(newUser);
  }

  async findAll() {
    return this.userRepo.find();
  }

  async findOne(id: number) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      return user;
    } else {
      throw new NotFoundException('User not found');
    }
  }
  async findByUserNameOrEmail(username: string) {
    const user = await this.userRepo.findOne({
      where: [{ username }, { email: username }],
    });

    return user;
  }
  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      this.userRepo.merge(user, updateUserDto);
      return this.userRepo.save(user);
    } else {
      throw new NotFoundException('User not found');
    }
  }

  async remove(id: number) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      return this.userRepo.delete(id);
    } else {
      throw new NotFoundException('User not found');
    }
  }
}
